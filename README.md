# TrustYou Probation day

## About the technical test

### Using Cors anywhere

I'm using Cors anywhere to cross-origin requests to the API.

### Posible improvements

#### Use of a more dynamic library to show the content

I for this demo just used simple no-brainer libraries, like "react-minimal-pie-chart".
There are libraries out there that are way more dynamic and useful for showing information like charts, tables, etc.

#### Better content style

It would look way more integrated with the actual app if I used the app's style.

#### Accessibility and responsive

#### More testing

**E2E testing** is very important for apps like this one to make sure that the app does not fail and works as expected.

More **unit/component testing** to cover every possible case. Due it's just a demo, not even an MVP, I wrote very very little.

#### Git + CI/CD

#### Use of Trello to break down the task into smaller ones

#### environment files

## Available Scripts

### npm start

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### npm test

Launches the test runner in the interactive watch mode.
See the section about running tests for more information.

### npm run build

Builds a static copy of your site to the `build/` folder.
Your app is ready to be deployed!

**For the best production performance:** Add a build bundler plugin like "@snowpack/plugin-webpack" or "@snowpack/plugin-parcel" to your `snowpack.config.json` config file.

- - -

> ✨ Bootstrapped with Create Snowpack App (CSA).