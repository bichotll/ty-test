module.exports = {
  mount: {
    public: '/',
    src: '/_dist_',
  },
  plugins: [
    ['@snowpack/plugin-react-refresh', {
      "babel": false,
    }],
    '@snowpack/plugin-dotenv',
    ["@snowpack/plugin-run-script", {
      "cmd": "eslint 'src/**/*.{js,jsx,ts,tsx}'",
      // Optional: Use npm package "watch" to run on every file change
      "watch": "watch \"$1\" src"
    }],
    ["@snowpack/plugin-run-script", {
      "cmd": "node-sass -r src/css/ -o public/css/",
      "watch": "$1 --watch",
    }],
  ],
};
