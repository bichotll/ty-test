import axios from 'axios'

const baseUrl = 'https://cors-anywhere.herokuapp.com/https://front-end-technical-test-api.integration.eu.cloud.trustyou.net'


const TrustYou = {
  getReviewDistribution () {
    return axios.get(`${baseUrl}/review-distribution`)
  },

  getReviews (filter_type, filter_value) {
    return axios.get(`${baseUrl}/reviews`, {
      params: {
        filter_type,
        filter_value,
      }
    })
  },
}

export default TrustYou
