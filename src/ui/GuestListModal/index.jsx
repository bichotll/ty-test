import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'

import TrustYouApi from './../../services/trust-you-api'


class GuestListModal extends Component {
  state = {
    reviewsData: undefined,
  }

  componentDidMount() {
    const { filterType, filterValue } = this.props

    TrustYouApi.getReviews(filterType, filterValue).then(({ data }) => {
      this.setState({
        reviewsData: data.data
      })
    })
  }

  render() {
    const { onHide } = this.props
    const { reviewsData } = this.state

    return (
      <>
        <Modal
          show
          onHide={onHide}
          size="md"
        >
          <Modal.Header closeButton>
            <Modal.Title>Review list</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            {!reviewsData && (
              <div className="spinner-border mt-4" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            )}

            {reviewsData && (
              <table className="table table-hover text-left p-4 pb-0">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Creation time</th>
                  </tr>
                </thead>
                <tbody>
                  {reviewsData.map(({review_id, review_title, guest_name, creation_date}) => (
                    <tr
                      key={review_id}
                    >
                      <td className="text-truncate" style={{ maxWidth: '100px' }}>
                        {review_title}
                      </td>
                      <td>
                        {guest_name}
                      </td>
                      <td>
                        {(new Date(creation_date).toISOString())}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            )}
          </Modal.Body>
        </Modal>
      </>
    )
  }
}

export default GuestListModal
