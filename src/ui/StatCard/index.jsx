import React from 'react'
import { PieChart } from 'react-minimal-pie-chart'
import stc from 'string-to-color'


function StatCard({ type, data, onRowClick }) {
  const dataArray = []
  for (const key in data) {
    const value = data[key]
    dataArray.push({
      key,
      value,
      color: stc(key),
    })
  }
  dataArray.sort((a, b) => (b.key !== 'null' ? b.key : -1000) - (a.key !== 'null' ? a.key : -1000))

  return (
    <div className="card stat-card">
      <div
        className="card-header text-left"
        data-testid="card-header" 
      >
        {type.replace('_', ' ')}
      </div>
      <div className="card-body">
        <div className="row">
          <div className="col-5">
            <PieChart
              data={[
                ...dataArray.map(({ key, value, color }) => ({
                  title: key,
                  value,
                  color,
                })),
              ]}
            />
          </div>
          <div className="col-7">
            <table className="table table-hover text-left">
              <thead>
                <tr>
                  <th>*</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
                {dataArray.map(({key, value, color}) => (
                  <tr
                    key={key}
                    data-testid="table-row"
                    onClick={() => {
                      onRowClick(type, key)
                    }}
                  >
                    <th
                      style={{color}}
                    >
                      {key === 'null' ? '?' : key}
                    </th>
                    <td>{value}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default StatCard
