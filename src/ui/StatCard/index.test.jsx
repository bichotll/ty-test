import * as React from 'react';
import { render } from '@testing-library/react';

import StatCard from './';


const type = 'fdsa_fdsa'
const data = {
  '1t': 1,
  '2n': 2,
  '3t': 3,
}


describe('StatCard', () => {
  describe('header', () => {
    it('replaces "_" by " "', () => {
      
      const { getByTestId } = render(<StatCard type={type} />)
      const cardHeader = getByTestId('card-header')
      expect(cardHeader).toHaveTextContent('fdsa fdsa')
    })
  })

  describe('table', () => {
    it('prints rows as many elements in the data prop', () => {
      const { getAllByTestId } = render(<StatCard type={type} data={data} />)
      const tableRows = getAllByTestId('table-row')
      expect(tableRows).toHaveLength(3)
    })
  })
})
