import React, { Component } from 'react'

import TrustYouApi from './../../../services/trust-you-api'
import StatCard from './../../StatCard'
import GuestListModal from './../../GuestListModal'


class Home extends Component {
  state = {
    reviewsData: undefined,
    modalListShown: false,
    filterType: undefined,
    filterValue: undefined,
  }

  componentDidMount() {
    TrustYouApi.getReviewDistribution().then(({ data }) => {
      this.setState({
        reviewsData: data.data
      })
    })
  }

  showModalList = (filterType, filterValue) => {
    this.setState({
      filterType,
      filterValue,
      modalListShown: true,
    })
  }

  hideModalList = () => {
    this.setState({
      modalListShown: false,
    })
  }

  render() {
    const { reviewsData, modalListShown, filterType, filterValue } = this.state

    const reviewsNode = []
    if (reviewsData) {
      for (const reviewName in reviewsData) {
        const reviewData = reviewsData[reviewName];
  
        reviewsNode.push(

              <StatCard
                type={reviewName}
                data={reviewData}
                onRowClick={this.showModalList}
              />

        )
      }
    }

    return (
      <>
        {modalListShown && (
          <GuestListModal
            onHide={this.hideModalList}
            filterType={filterType}
            filterValue={filterValue}
          />
        )}

        <div className="container pt-4 pb-4">
          {!reviewsData && (
            <div className="spinner-border mt-4" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          )}

          {reviewsData && (
            <div className="card-columns">
              {reviewsNode}
            </div>
          )}
        </div>
      </>
    )
  }
}

export default Home
